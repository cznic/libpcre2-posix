// Code generated for linux/amd64 by 'generator --libc modernc.org/libc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -o libpcre2.go --package-name libpcre2_posix .libs/libpcre2-posix.a -lpcre2-8', DO NOT EDIT.

//go:build linux && amd64

package libpcre2_posix

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libpcre2-8"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_ARG_MAX = 131072
const m_BC_BASE_MAX = 99
const m_BC_DIM_MAX = 2048
const m_BC_SCALE_MAX = 99
const m_BC_STRING_MAX = 1000
const m_BUFSIZ = 1024
const m_CHARCLASS_NAME_MAX = 14
const m_CHAR_BIT = 8
const m_CHAR_MAX = 255
const m_CHAR_MIN = 0
const m_COLL_WEIGHTS_MAX = 2
const m_COMPILE_ERROR_BASE = 100
const m_DELAYTIMER_MAX = 0x7fffffff
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_EXPR_NEST_MAX = 32
const m_FILENAME_MAX = 4096
const m_FILESIZEBITS = 64
const m_FOPEN_MAX = 1000
const m_HAVE_ATTRIBUTE_UNINITIALIZED = 1
const m_HAVE_BCOPY = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DIRENT_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_LIMITS_H = 1
const m_HAVE_MEMFD_CREATE = 1
const m_HAVE_MEMMOVE = 1
const m_HAVE_MKOSTEMP = 1
const m_HAVE_REALPATH = 1
const m_HAVE_SECURE_GETENV = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRERROR = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_SYS_WAIT_H = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VISIBILITY = 1
const m_HAVE_WCHAR_H = 1
const m_HAVE_ZLIB_H = 1
const m_HEAP_LIMIT = 20000000
const m_HOST_NAME_MAX = 255
const m_INT16_MAX = 0x7fff
const m_INT32_MAX = 0x7fffffff
const m_INT64_MAX = 0x7fffffffffffffff
const m_INT8_MAX = 0x7f
const m_INTMAX_MAX = "INT64_MAX"
const m_INTMAX_MIN = "INT64_MIN"
const m_INTPTR_MAX = "INT64_MAX"
const m_INTPTR_MIN = "INT64_MIN"
const m_INT_FAST16_MAX = "INT32_MAX"
const m_INT_FAST16_MIN = "INT32_MIN"
const m_INT_FAST32_MAX = "INT32_MAX"
const m_INT_FAST32_MIN = "INT32_MIN"
const m_INT_FAST64_MAX = "INT64_MAX"
const m_INT_FAST64_MIN = "INT64_MIN"
const m_INT_FAST8_MAX = "INT8_MAX"
const m_INT_FAST8_MIN = "INT8_MIN"
const m_INT_LEAST16_MAX = "INT16_MAX"
const m_INT_LEAST16_MIN = "INT16_MIN"
const m_INT_LEAST32_MAX = "INT32_MAX"
const m_INT_LEAST32_MIN = "INT32_MIN"
const m_INT_LEAST64_MAX = "INT64_MAX"
const m_INT_LEAST64_MIN = "INT64_MIN"
const m_INT_LEAST8_MAX = "INT8_MAX"
const m_INT_LEAST8_MIN = "INT8_MIN"
const m_INT_MAX = 0x7fffffff
const m_IOV_MAX = 1024
const m_LINE_MAX = 4096
const m_LINK_SIZE = 2
const m_LLONG_MAX = 0x7fffffffffffffff
const m_LOGIN_NAME_MAX = 256
const m_LONG_BIT = 64
const m_LONG_MAX = "__LONG_MAX"
const m_LT_OBJDIR = ".libs/"
const m_L_ctermid = 20
const m_L_cuserid = 20
const m_L_tmpnam = 20
const m_MATCH_LIMIT = 10000000
const m_MATCH_LIMIT_DEPTH = "MATCH_LIMIT"
const m_MAX_NAME_COUNT = 10000
const m_MAX_NAME_SIZE = 32
const m_MB_LEN_MAX = 4
const m_MQ_PRIO_MAX = 32768
const m_NAME_MAX = 255
const m_NEWLINE_DEFAULT = 2
const m_NGROUPS_MAX = 32
const m_NL_ARGMAX = 9
const m_NL_LANGMAX = 32
const m_NL_MSGMAX = 32767
const m_NL_NMAX = 16
const m_NL_SETMAX = 255
const m_NL_TEXTMAX = 2048
const m_NZERO = 20
const m_PACKAGE = "pcre2"
const m_PACKAGE_BUGREPORT = ""
const m_PACKAGE_NAME = "PCRE2"
const m_PACKAGE_STRING = "PCRE2 10.42"
const m_PACKAGE_TARNAME = "pcre2"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "10.42"
const m_PAGESIZE = 4096
const m_PAGE_SIZE = "PAGESIZE"
const m_PARENS_NEST_LIMIT = 250
const m_PATH_MAX = 4096
const m_PCRE2GREP_BUFSIZE = 20480
const m_PCRE2GREP_MAX_BUFSIZE = 1048576
const m_PCRE2_ALLOW_EMPTY_CLASS = "0x00000001u"
const m_PCRE2_ALT_BSUX = "0x00000002u"
const m_PCRE2_ALT_CIRCUMFLEX = "0x00200000u"
const m_PCRE2_ALT_VERBNAMES = "0x00400000u"
const m_PCRE2_ANCHORED = "0x80000000u"
const m_PCRE2_AUTO_CALLOUT = "0x00000004u"
const m_PCRE2_BSR_ANYCRLF = 2
const m_PCRE2_BSR_UNICODE = 1
const m_PCRE2_CALLOUT_BACKTRACK = "0x00000002u"
const m_PCRE2_CALLOUT_STARTMATCH = "0x00000001u"
const m_PCRE2_CASELESS = 8
const m_PCRE2_CODE_UNIT_WIDTH = 8
const m_PCRE2_CONFIG_BSR = 0
const m_PCRE2_CONFIG_COMPILED_WIDTHS = 14
const m_PCRE2_CONFIG_DEPTHLIMIT = 7
const m_PCRE2_CONFIG_HEAPLIMIT = 12
const m_PCRE2_CONFIG_JIT = 1
const m_PCRE2_CONFIG_JITTARGET = 2
const m_PCRE2_CONFIG_LINKSIZE = 3
const m_PCRE2_CONFIG_MATCHLIMIT = 4
const m_PCRE2_CONFIG_NEVER_BACKSLASH_C = 13
const m_PCRE2_CONFIG_NEWLINE = 5
const m_PCRE2_CONFIG_PARENSLIMIT = 6
const m_PCRE2_CONFIG_RECURSIONLIMIT = 7
const m_PCRE2_CONFIG_STACKRECURSE = 8
const m_PCRE2_CONFIG_TABLES_LENGTH = 15
const m_PCRE2_CONFIG_UNICODE = 9
const m_PCRE2_CONFIG_UNICODE_VERSION = 10
const m_PCRE2_CONFIG_VERSION = 11
const m_PCRE2_CONVERT_GLOB = "0x00000010u"
const m_PCRE2_CONVERT_GLOB_NO_STARSTAR = "0x00000050u"
const m_PCRE2_CONVERT_GLOB_NO_WILD_SEPARATOR = "0x00000030u"
const m_PCRE2_CONVERT_NO_UTF_CHECK = "0x00000002u"
const m_PCRE2_CONVERT_POSIX_BASIC = "0x00000004u"
const m_PCRE2_CONVERT_POSIX_EXTENDED = "0x00000008u"
const m_PCRE2_CONVERT_UTF = "0x00000001u"
const m_PCRE2_COPY_MATCHED_SUBJECT = "0x00004000u"
const m_PCRE2_DFA_RESTART = "0x00000040u"
const m_PCRE2_DFA_SHORTEST = "0x00000080u"
const m_PCRE2_DOLLAR_ENDONLY = "0x00000010u"
const m_PCRE2_DOTALL = 32
const m_PCRE2_DUPNAMES = "0x00000040u"
const m_PCRE2_ENDANCHORED = "0x20000000u"
const m_PCRE2_ERROR_ALPHA_ASSERTION_UNKNOWN = 195
const m_PCRE2_ERROR_BACKSLASH_C_CALLER_DISABLED = 183
const m_PCRE2_ERROR_BACKSLASH_C_LIBRARY_DISABLED = 185
const m_PCRE2_ERROR_BACKSLASH_C_SYNTAX = 168
const m_PCRE2_ERROR_BACKSLASH_G_SYNTAX = 157
const m_PCRE2_ERROR_BACKSLASH_K_IN_LOOKAROUND = 199
const m_PCRE2_ERROR_BACKSLASH_K_SYNTAX = 169
const m_PCRE2_ERROR_BACKSLASH_N_IN_CLASS = 171
const m_PCRE2_ERROR_BACKSLASH_O_MISSING_BRACE = 155
const m_PCRE2_ERROR_BACKSLASH_U_CODE_POINT_TOO_BIG = 177
const m_PCRE2_ERROR_BAD_LITERAL_OPTIONS = 192
const m_PCRE2_ERROR_BAD_OPTIONS = 117
const m_PCRE2_ERROR_BAD_RELATIVE_REFERENCE = 129
const m_PCRE2_ERROR_BAD_SUBPATTERN_REFERENCE = 115
const m_PCRE2_ERROR_CALLOUT_BAD_STRING_DELIMITER = 182
const m_PCRE2_ERROR_CALLOUT_NO_STRING_DELIMITER = 181
const m_PCRE2_ERROR_CALLOUT_NUMBER_TOO_BIG = 138
const m_PCRE2_ERROR_CALLOUT_STRING_TOO_LONG = 172
const m_PCRE2_ERROR_CLASS_INVALID_RANGE = 150
const m_PCRE2_ERROR_CLASS_RANGE_ORDER = 108
const m_PCRE2_ERROR_CODE_POINT_TOO_BIG = 134
const m_PCRE2_ERROR_CONDITION_ASSERTION_EXPECTED = 128
const m_PCRE2_ERROR_CONDITION_ATOMIC_ASSERTION_EXPECTED = 198
const m_PCRE2_ERROR_DEFINE_TOO_MANY_BRANCHES = 154
const m_PCRE2_ERROR_DUPLICATE_SUBPATTERN_NAME = 143
const m_PCRE2_ERROR_END_BACKSLASH = 101
const m_PCRE2_ERROR_END_BACKSLASH_C = 102
const m_PCRE2_ERROR_ESCAPE_INVALID_IN_CLASS = 107
const m_PCRE2_ERROR_ESCAPE_INVALID_IN_VERB = 140
const m_PCRE2_ERROR_HEAP_FAILED = 121
const m_PCRE2_ERROR_INTERNAL_BAD_CODE = 189
const m_PCRE2_ERROR_INTERNAL_BAD_CODE_AUTO_POSSESS = 180
const m_PCRE2_ERROR_INTERNAL_BAD_CODE_IN_SKIP = 190
const m_PCRE2_ERROR_INTERNAL_BAD_CODE_LOOKBEHINDS = 170
const m_PCRE2_ERROR_INTERNAL_CODE_OVERFLOW = 123
const m_PCRE2_ERROR_INTERNAL_MISSING_SUBPATTERN = 153
const m_PCRE2_ERROR_INTERNAL_OVERRAN_WORKSPACE = 152
const m_PCRE2_ERROR_INTERNAL_PARSED_OVERFLOW = 163
const m_PCRE2_ERROR_INTERNAL_STUDY_ERROR = 131
const m_PCRE2_ERROR_INTERNAL_UNEXPECTED_REPEAT = 110
const m_PCRE2_ERROR_INTERNAL_UNKNOWN_NEWLINE = 156
const m_PCRE2_ERROR_INVALID_AFTER_PARENS_QUERY = 111
const m_PCRE2_ERROR_INVALID_HEXADECIMAL = 167
const m_PCRE2_ERROR_INVALID_HYPHEN_IN_OPTIONS = 194
const m_PCRE2_ERROR_INVALID_OCTAL = 164
const m_PCRE2_ERROR_INVALID_SUBPATTERN_NAME = 144
const m_PCRE2_ERROR_LOOKBEHIND_INVALID_BACKSLASH_C = 136
const m_PCRE2_ERROR_LOOKBEHIND_NOT_FIXED_LENGTH = 125
const m_PCRE2_ERROR_LOOKBEHIND_TOO_COMPLICATED = 135
const m_PCRE2_ERROR_LOOKBEHIND_TOO_LONG = 187
const m_PCRE2_ERROR_MALFORMED_UNICODE_PROPERTY = 146
const m_PCRE2_ERROR_MARK_MISSING_ARGUMENT = 166
const m_PCRE2_ERROR_MISSING_CALLOUT_CLOSING = 139
const m_PCRE2_ERROR_MISSING_CLOSING_PARENTHESIS = 114
const m_PCRE2_ERROR_MISSING_COMMENT_CLOSING = 118
const m_PCRE2_ERROR_MISSING_CONDITION_CLOSING = 124
const m_PCRE2_ERROR_MISSING_NAME_TERMINATOR = 142
const m_PCRE2_ERROR_MISSING_OCTAL_OR_HEX_DIGITS = 178
const m_PCRE2_ERROR_MISSING_SQUARE_BRACKET = 106
const m_PCRE2_ERROR_NO_SURROGATES_IN_UTF16 = 191
const m_PCRE2_ERROR_NULL_PATTERN = 116
const m_PCRE2_ERROR_OCTAL_BYTE_TOO_BIG = 151
const m_PCRE2_ERROR_PARENS_QUERY_R_MISSING_CLOSING = 158
const m_PCRE2_ERROR_PARENTHESES_NEST_TOO_DEEP = 119
const m_PCRE2_ERROR_PARENTHESES_STACK_CHECK = 133
const m_PCRE2_ERROR_PATTERN_STRING_TOO_LONG = 188
const m_PCRE2_ERROR_PATTERN_TOO_COMPLICATED = 186
const m_PCRE2_ERROR_PATTERN_TOO_LARGE = 120
const m_PCRE2_ERROR_POSIX_CLASS_NOT_IN_CLASS = 112
const m_PCRE2_ERROR_POSIX_NO_SUPPORT_COLLATING = 113
const m_PCRE2_ERROR_QUANTIFIER_INVALID = 109
const m_PCRE2_ERROR_QUANTIFIER_OUT_OF_ORDER = 104
const m_PCRE2_ERROR_QUANTIFIER_TOO_BIG = 105
const m_PCRE2_ERROR_QUERY_BARJX_NEST_TOO_DEEP = 184
const m_PCRE2_ERROR_SCRIPT_RUN_NOT_AVAILABLE = 196
const m_PCRE2_ERROR_SUBPATTERN_NAMES_MISMATCH = 165
const m_PCRE2_ERROR_SUBPATTERN_NAME_EXPECTED = 162
const m_PCRE2_ERROR_SUBPATTERN_NAME_TOO_LONG = 148
const m_PCRE2_ERROR_SUBPATTERN_NUMBER_TOO_BIG = 161
const m_PCRE2_ERROR_SUPPORTED_ONLY_IN_UNICODE = 193
const m_PCRE2_ERROR_TOO_MANY_CAPTURES = 197
const m_PCRE2_ERROR_TOO_MANY_CONDITION_BRANCHES = 127
const m_PCRE2_ERROR_TOO_MANY_NAMED_SUBPATTERNS = 149
const m_PCRE2_ERROR_UCP_IS_DISABLED = 175
const m_PCRE2_ERROR_UNICODE_DISALLOWED_CODE_POINT = 173
const m_PCRE2_ERROR_UNICODE_NOT_SUPPORTED = 132
const m_PCRE2_ERROR_UNICODE_PROPERTIES_UNAVAILABLE = 145
const m_PCRE2_ERROR_UNKNOWN_ESCAPE = 103
const m_PCRE2_ERROR_UNKNOWN_POSIX_CLASS = 130
const m_PCRE2_ERROR_UNKNOWN_UNICODE_PROPERTY = 147
const m_PCRE2_ERROR_UNMATCHED_CLOSING_PARENTHESIS = 122
const m_PCRE2_ERROR_UNRECOGNIZED_AFTER_QUERY_P = 141
const m_PCRE2_ERROR_UNSUPPORTED_ESCAPE_SEQUENCE = 137
const m_PCRE2_ERROR_UTF_IS_DISABLED = 174
const m_PCRE2_ERROR_VERB_ARGUMENT_NOT_ALLOWED = 159
const m_PCRE2_ERROR_VERB_NAME_TOO_LONG = 176
const m_PCRE2_ERROR_VERB_UNKNOWN = 160
const m_PCRE2_ERROR_VERSION_CONDITION_SYNTAX = 179
const m_PCRE2_ERROR_ZERO_RELATIVE_REFERENCE = 126
const m_PCRE2_EXTENDED = "0x00000080u"
const m_PCRE2_EXTENDED_MORE = "0x01000000u"
const m_PCRE2_EXTRA_ALLOW_LOOKAROUND_BSK = "0x00000040u"
const m_PCRE2_EXTRA_ALLOW_SURROGATE_ESCAPES = "0x00000001u"
const m_PCRE2_EXTRA_ALT_BSUX = "0x00000020u"
const m_PCRE2_EXTRA_BAD_ESCAPE_IS_LITERAL = "0x00000002u"
const m_PCRE2_EXTRA_ESCAPED_CR_IS_LF = "0x00000010u"
const m_PCRE2_EXTRA_MATCH_LINE = "0x00000008u"
const m_PCRE2_EXTRA_MATCH_WORD = "0x00000004u"
const m_PCRE2_FIRSTLINE = "0x00000100u"
const m_PCRE2_INFO_ALLOPTIONS = 0
const m_PCRE2_INFO_ARGOPTIONS = 1
const m_PCRE2_INFO_BACKREFMAX = 2
const m_PCRE2_INFO_BSR = 3
const m_PCRE2_INFO_CAPTURECOUNT = 4
const m_PCRE2_INFO_DEPTHLIMIT = 21
const m_PCRE2_INFO_EXTRAOPTIONS = 26
const m_PCRE2_INFO_FIRSTBITMAP = 7
const m_PCRE2_INFO_FIRSTCODETYPE = 6
const m_PCRE2_INFO_FIRSTCODEUNIT = 5
const m_PCRE2_INFO_FRAMESIZE = 24
const m_PCRE2_INFO_HASBACKSLASHC = 23
const m_PCRE2_INFO_HASCRORLF = 8
const m_PCRE2_INFO_HEAPLIMIT = 25
const m_PCRE2_INFO_JCHANGED = 9
const m_PCRE2_INFO_JITSIZE = 10
const m_PCRE2_INFO_LASTCODETYPE = 12
const m_PCRE2_INFO_LASTCODEUNIT = 11
const m_PCRE2_INFO_MATCHEMPTY = 13
const m_PCRE2_INFO_MATCHLIMIT = 14
const m_PCRE2_INFO_MAXLOOKBEHIND = 15
const m_PCRE2_INFO_MINLENGTH = 16
const m_PCRE2_INFO_NAMECOUNT = 17
const m_PCRE2_INFO_NAMEENTRYSIZE = 18
const m_PCRE2_INFO_NAMETABLE = 19
const m_PCRE2_INFO_NEWLINE = 20
const m_PCRE2_INFO_RECURSIONLIMIT = 21
const m_PCRE2_INFO_SIZE = 22
const m_PCRE2_JIT_COMPLETE = "0x00000001u"
const m_PCRE2_JIT_INVALID_UTF = "0x00000100u"
const m_PCRE2_JIT_PARTIAL_HARD = "0x00000004u"
const m_PCRE2_JIT_PARTIAL_SOFT = "0x00000002u"
const m_PCRE2_LITERAL = 33554432
const m_PCRE2_MAJOR = 10
const m_PCRE2_MATCH_INVALID_UTF = "0x04000000u"
const m_PCRE2_MATCH_UNSET_BACKREF = "0x00000200u"
const m_PCRE2_MINOR = 42
const m_PCRE2_MULTILINE = 1024
const m_PCRE2_NEVER_BACKSLASH_C = "0x00100000u"
const m_PCRE2_NEVER_UCP = "0x00000800u"
const m_PCRE2_NEVER_UTF = "0x00001000u"
const m_PCRE2_NEWLINE_ANY = 4
const m_PCRE2_NEWLINE_ANYCRLF = 5
const m_PCRE2_NEWLINE_CR = 1
const m_PCRE2_NEWLINE_CRLF = 3
const m_PCRE2_NEWLINE_LF = 2
const m_PCRE2_NEWLINE_NUL = 6
const m_PCRE2_NOTBOL = 1
const m_PCRE2_NOTEMPTY = 4
const m_PCRE2_NOTEMPTY_ATSTART = "0x00000008u"
const m_PCRE2_NOTEOL = 2
const m_PCRE2_NO_AUTO_CAPTURE = "0x00002000u"
const m_PCRE2_NO_AUTO_POSSESS = "0x00004000u"
const m_PCRE2_NO_DOTSTAR_ANCHOR = "0x00008000u"
const m_PCRE2_NO_JIT = "0x00002000u"
const m_PCRE2_NO_START_OPTIMIZE = "0x00010000u"
const m_PCRE2_NO_UTF_CHECK = "0x40000000u"
const m_PCRE2_PARTIAL_HARD = "0x00000020u"
const m_PCRE2_PARTIAL_SOFT = "0x00000010u"
const m_PCRE2_SIZE = "size_t"
const m_PCRE2_SIZE_MAX = "SIZE_MAX"
const m_PCRE2_STATIC = 1
const m_PCRE2_SUBSTITUTE_EXTENDED = "0x00000200u"
const m_PCRE2_SUBSTITUTE_GLOBAL = "0x00000100u"
const m_PCRE2_SUBSTITUTE_LITERAL = "0x00008000u"
const m_PCRE2_SUBSTITUTE_MATCHED = "0x00010000u"
const m_PCRE2_SUBSTITUTE_OVERFLOW_LENGTH = "0x00001000u"
const m_PCRE2_SUBSTITUTE_REPLACEMENT_ONLY = "0x00020000u"
const m_PCRE2_SUBSTITUTE_UNKNOWN_UNSET = "0x00000800u"
const m_PCRE2_SUBSTITUTE_UNSET_EMPTY = "0x00000400u"
const m_PCRE2_UCP = 131072
const m_PCRE2_UNGREEDY = 262144
const m_PCRE2_USE_OFFSET_LIMIT = "0x00800000u"
const m_PCRE2_UTF = 524288
const m_PCRE2regcomp = "pcre2_regcomp"
const m_PCRE2regerror = "pcre2_regerror"
const m_PCRE2regexec = "pcre2_regexec"
const m_PCRE2regfree = "pcre2_regfree"
const m_PIPE_BUF = 4096
const m_PRIX16 = "X"
const m_PRIX32 = "X"
const m_PRIX8 = "X"
const m_PRIXFAST16 = "X"
const m_PRIXFAST32 = "X"
const m_PRIXFAST8 = "X"
const m_PRIXLEAST16 = "X"
const m_PRIXLEAST32 = "X"
const m_PRIXLEAST8 = "X"
const m_PRId16 = "d"
const m_PRId32 = "d"
const m_PRId8 = "d"
const m_PRIdFAST16 = "d"
const m_PRIdFAST32 = "d"
const m_PRIdFAST8 = "d"
const m_PRIdLEAST16 = "d"
const m_PRIdLEAST32 = "d"
const m_PRIdLEAST8 = "d"
const m_PRIi16 = "i"
const m_PRIi32 = "i"
const m_PRIi8 = "i"
const m_PRIiFAST16 = "i"
const m_PRIiFAST32 = "i"
const m_PRIiFAST8 = "i"
const m_PRIiLEAST16 = "i"
const m_PRIiLEAST32 = "i"
const m_PRIiLEAST8 = "i"
const m_PRIo16 = "o"
const m_PRIo32 = "o"
const m_PRIo8 = "o"
const m_PRIoFAST16 = "o"
const m_PRIoFAST32 = "o"
const m_PRIoFAST8 = "o"
const m_PRIoLEAST16 = "o"
const m_PRIoLEAST32 = "o"
const m_PRIoLEAST8 = "o"
const m_PRIu16 = "u"
const m_PRIu32 = "u"
const m_PRIu8 = "u"
const m_PRIuFAST16 = "u"
const m_PRIuFAST32 = "u"
const m_PRIuFAST8 = "u"
const m_PRIuLEAST16 = "u"
const m_PRIuLEAST32 = "u"
const m_PRIuLEAST8 = "u"
const m_PRIx16 = "x"
const m_PRIx32 = "x"
const m_PRIx8 = "x"
const m_PRIxFAST16 = "x"
const m_PRIxFAST32 = "x"
const m_PRIxFAST8 = "x"
const m_PRIxLEAST16 = "x"
const m_PRIxLEAST32 = "x"
const m_PRIxLEAST8 = "x"
const m_PTHREAD_DESTRUCTOR_ITERATIONS = 4
const m_PTHREAD_KEYS_MAX = 128
const m_PTHREAD_STACK_MIN = 2048
const m_PTRDIFF_MAX = "INT64_MAX"
const m_PTRDIFF_MIN = "INT64_MIN"
const m_P_tmpdir = "/tmp"
const m_RAND_MAX = 0x7fffffff
const m_REG_DOTALL = 16
const m_REG_EXTENDED = 0
const m_REG_ICASE = 1
const m_REG_NEWLINE = 2
const m_REG_NOSPEC = 4096
const m_REG_NOSUB = 32
const m_REG_NOTBOL = 4
const m_REG_NOTEMPTY = 256
const m_REG_NOTEOL = 8
const m_REG_PEND = 2048
const m_REG_STARTEND = 128
const m_REG_UCP = 1024
const m_REG_UNGREEDY = 512
const m_REG_UTF = 64
const m_RE_DUP_MAX = 255
const m_SCHAR_MAX = 127
const m_SCNd16 = "hd"
const m_SCNd32 = "d"
const m_SCNd8 = "hhd"
const m_SCNdFAST16 = "d"
const m_SCNdFAST32 = "d"
const m_SCNdFAST8 = "hhd"
const m_SCNdLEAST16 = "hd"
const m_SCNdLEAST32 = "d"
const m_SCNdLEAST8 = "hhd"
const m_SCNi16 = "hi"
const m_SCNi32 = "i"
const m_SCNi8 = "hhi"
const m_SCNiFAST16 = "i"
const m_SCNiFAST32 = "i"
const m_SCNiFAST8 = "hhi"
const m_SCNiLEAST16 = "hi"
const m_SCNiLEAST32 = "i"
const m_SCNiLEAST8 = "hhi"
const m_SCNo16 = "ho"
const m_SCNo32 = "o"
const m_SCNo8 = "hho"
const m_SCNoFAST16 = "o"
const m_SCNoFAST32 = "o"
const m_SCNoFAST8 = "hho"
const m_SCNoLEAST16 = "ho"
const m_SCNoLEAST32 = "o"
const m_SCNoLEAST8 = "hho"
const m_SCNu16 = "hu"
const m_SCNu32 = "u"
const m_SCNu8 = "hhu"
const m_SCNuFAST16 = "u"
const m_SCNuFAST32 = "u"
const m_SCNuFAST8 = "hhu"
const m_SCNuLEAST16 = "hu"
const m_SCNuLEAST32 = "u"
const m_SCNuLEAST8 = "hhu"
const m_SCNx16 = "hx"
const m_SCNx32 = "x"
const m_SCNx8 = "hhx"
const m_SCNxFAST16 = "x"
const m_SCNxFAST32 = "x"
const m_SCNxFAST8 = "hhx"
const m_SCNxLEAST16 = "hx"
const m_SCNxLEAST32 = "x"
const m_SCNxLEAST8 = "hhx"
const m_SEM_NSEMS_MAX = 256
const m_SEM_VALUE_MAX = 0x7fffffff
const m_SHRT_MAX = 0x7fff
const m_SIG_ATOMIC_MAX = "INT32_MAX"
const m_SIG_ATOMIC_MIN = "INT32_MIN"
const m_SIZE_MAX = "UINT64_MAX"
const m_SSIZE_MAX = "LONG_MAX"
const m_STDC_HEADERS = 1
const m_SYMLOOP_MAX = 40
const m_TMP_MAX = 10000
const m_TTY_NAME_MAX = 32
const m_TZNAME_MAX = 6
const m_UCHAR_MAX = 255
const m_UINT16_MAX = 0xffff
const m_UINT32_MAX = "0xffffffffu"
const m_UINT64_MAX = "0xffffffffffffffffu"
const m_UINT8_MAX = 0xff
const m_UINTMAX_MAX = "UINT64_MAX"
const m_UINTPTR_MAX = "UINT64_MAX"
const m_UINT_FAST16_MAX = "UINT32_MAX"
const m_UINT_FAST32_MAX = "UINT32_MAX"
const m_UINT_FAST64_MAX = "UINT64_MAX"
const m_UINT_FAST8_MAX = "UINT8_MAX"
const m_UINT_LEAST16_MAX = "UINT16_MAX"
const m_UINT_LEAST32_MAX = "UINT32_MAX"
const m_UINT_LEAST64_MAX = "UINT64_MAX"
const m_UINT_LEAST8_MAX = "UINT8_MAX"
const m_UINT_MAX = 0xffffffff
const m_USHRT_MAX = 0xffff
const m_VERSION = "10.42"
const m_WINT_MAX = "UINT32_MAX"
const m_WINT_MIN = 0
const m_WNOHANG = 1
const m_WORD_BIT = 32
const m_WUNTRACED = 2
const m__ALL_SOURCE = 1
const m__DARWIN_C_SOURCE = 1
const m__GNU_SOURCE = 1
const m__HPUX_ALT_XOPEN_SOCKET_API = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__LP64 = 1
const m__NETBSD_SOURCE = 1
const m__OPENBSD_SOURCE = 1
const m__POSIX2_BC_BASE_MAX = 99
const m__POSIX2_BC_DIM_MAX = 2048
const m__POSIX2_BC_SCALE_MAX = 99
const m__POSIX2_BC_STRING_MAX = 1000
const m__POSIX2_CHARCLASS_NAME_MAX = 14
const m__POSIX2_COLL_WEIGHTS_MAX = 2
const m__POSIX2_EXPR_NEST_MAX = 32
const m__POSIX2_LINE_MAX = 2048
const m__POSIX2_RE_DUP_MAX = 255
const m__POSIX_AIO_LISTIO_MAX = 2
const m__POSIX_AIO_MAX = 1
const m__POSIX_ARG_MAX = 4096
const m__POSIX_CHILD_MAX = 25
const m__POSIX_CLOCKRES_MIN = 20000000
const m__POSIX_DELAYTIMER_MAX = 32
const m__POSIX_HOST_NAME_MAX = 255
const m__POSIX_LINK_MAX = 8
const m__POSIX_LOGIN_NAME_MAX = 9
const m__POSIX_MAX_CANON = 255
const m__POSIX_MAX_INPUT = 255
const m__POSIX_MQ_OPEN_MAX = 8
const m__POSIX_MQ_PRIO_MAX = 32
const m__POSIX_NAME_MAX = 14
const m__POSIX_NGROUPS_MAX = 8
const m__POSIX_OPEN_MAX = 20
const m__POSIX_PATH_MAX = 256
const m__POSIX_PIPE_BUF = 512
const m__POSIX_PTHREAD_SEMANTICS = 1
const m__POSIX_RE_DUP_MAX = 255
const m__POSIX_RTSIG_MAX = 8
const m__POSIX_SEM_NSEMS_MAX = 256
const m__POSIX_SEM_VALUE_MAX = 32767
const m__POSIX_SIGQUEUE_MAX = 32
const m__POSIX_SSIZE_MAX = 32767
const m__POSIX_SS_REPL_MAX = 4
const m__POSIX_STREAM_MAX = 8
const m__POSIX_SYMLINK_MAX = 255
const m__POSIX_SYMLOOP_MAX = 8
const m__POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const m__POSIX_THREAD_KEYS_MAX = 128
const m__POSIX_THREAD_THREADS_MAX = 64
const m__POSIX_TIMER_MAX = 32
const m__POSIX_TRACE_EVENT_NAME_MAX = 30
const m__POSIX_TRACE_NAME_MAX = 8
const m__POSIX_TRACE_SYS_MAX = 8
const m__POSIX_TRACE_USER_EVENT_MAX = 32
const m__POSIX_TTY_NAME_MAX = 9
const m__POSIX_TZNAME_MAX = 6
const m__STDC_PREDEF_H = 1
const m__TANDEM_SOURCE = 1
const m__XOPEN_IOV_MAX = 16
const m__XOPEN_NAME_MAX = 255
const m__XOPEN_PATH_MAX = 1024
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_HLE_ACQUIRE = 65536
const m___ATOMIC_HLE_RELEASE = 131072
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BIG_ENDIAN = 4321
const m___BYTE_ORDER = 1234
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_IS_IEC_60559__ = 2
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DEC128_EPSILON__ = 1e-33
const m___DEC128_MANT_DIG__ = 34
const m___DEC128_MAX_EXP__ = 6145
const m___DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const m___DEC128_MIN__ = 1e-6143
const m___DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const m___DEC32_EPSILON__ = 1e-6
const m___DEC32_MANT_DIG__ = 7
const m___DEC32_MAX_EXP__ = 97
const m___DEC32_MAX__ = 9.999999e96
const m___DEC32_MIN__ = 1e-95
const m___DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const m___DEC64_EPSILON__ = 1e-15
const m___DEC64_MANT_DIG__ = 16
const m___DEC64_MAX_EXP__ = 385
const m___DEC64_MAX__ = "9.999999999999999E384"
const m___DEC64_MIN__ = 1e-383
const m___DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const m___DECIMAL_BID_FORMAT__ = 1
const m___DECIMAL_DIG__ = 17
const m___DEC_EVAL_METHOD__ = 2
const m___ELF__ = 1
const m___EXTENSIONS__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT128_DECIMAL_DIG__ = 36
const m___FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT128_DIG__ = 33
const m___FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT128_HAS_DENORM__ = 1
const m___FLT128_HAS_INFINITY__ = 1
const m___FLT128_HAS_QUIET_NAN__ = 1
const m___FLT128_IS_IEC_60559__ = 2
const m___FLT128_MANT_DIG__ = 113
const m___FLT128_MAX_10_EXP__ = 4932
const m___FLT128_MAX_EXP__ = 16384
const m___FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT16_DECIMAL_DIG__ = 5
const m___FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const m___FLT16_DIG__ = 3
const m___FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const m___FLT16_HAS_DENORM__ = 1
const m___FLT16_HAS_INFINITY__ = 1
const m___FLT16_HAS_QUIET_NAN__ = 1
const m___FLT16_IS_IEC_60559__ = 2
const m___FLT16_MANT_DIG__ = 11
const m___FLT16_MAX_10_EXP__ = 4
const m___FLT16_MAX_EXP__ = 16
const m___FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const m___FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_IS_IEC_60559__ = 2
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_IS_IEC_60559__ = 2
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT64X_DECIMAL_DIG__ = 36
const m___FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT64X_DIG__ = 33
const m___FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT64X_HAS_DENORM__ = 1
const m___FLT64X_HAS_INFINITY__ = 1
const m___FLT64X_HAS_QUIET_NAN__ = 1
const m___FLT64X_IS_IEC_60559__ = 2
const m___FLT64X_MANT_DIG__ = 113
const m___FLT64X_MAX_10_EXP__ = 4932
const m___FLT64X_MAX_EXP__ = 16384
const m___FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_IS_IEC_60559__ = 2
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_IS_IEC_60559__ = 2
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_RADIX__ = 2
const m___FUNCTION__ = "__func__"
const m___FXSR__ = 1
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_CONSTRUCTIVE_SIZE = 64
const m___GCC_DESTRUCTIVE_SIZE = 64
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 0
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const m___GNUC__ = 12
const m___GXX_ABI_VERSION = 1017
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_IS_IEC_60559__ = 2
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const m___LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LITTLE_ENDIAN = 1234
const m___LONG_DOUBLE_64__ = 1
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX = 0x7fffffffffffffff
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___MMX_WITH_SSE__ = 1
const m___MMX__ = 1
const m___NO_INLINE__ = 1
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PIC__ = 2
const m___PIE__ = 2
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PRI64 = "l"
const m___PRIPTR = "l"
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SEG_FS = 1
const m___SEG_GS = 1
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT128__ = 16
const m___SIZEOF_FLOAT80__ = 16
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___SSE2_MATH__ = 1
const m___SSE2__ = 1
const m___SSE_MATH__ = 1
const m___SSE__ = 1
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_IEC_60559_BFP__ = 201404
const m___STDC_IEC_60559_COMPLEX__ = 201404
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC_WANT_IEC_60559_ATTRIBS_EXT__ = 1
const m___STDC_WANT_IEC_60559_BFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_DFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_FUNCS_EXT__ = 1
const m___STDC_WANT_IEC_60559_TYPES_EXT__ = 1
const m___STDC_WANT_LIB_EXT2__ = 1
const m___STDC_WANT_MATH_SPEC_FUNCS__ = 1
const m___STDC__ = 1
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___USE_TIME_BITS64 = 1
const m___VERSION__ = "12.2.0"
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___amd64 = 1
const m___amd64__ = 1
const m___code_model_small__ = 1
const m___gnu_linux__ = 1
const m___inline = "inline"
const m___k8 = 1
const m___k8__ = 1
const m___linux = 1
const m___linux__ = 1
const m___pic__ = 2
const m___pie__ = 2
const m___restrict = "restrict"
const m___restrict_arr = "restrict"
const m___unix = 1
const m___unix__ = 1
const m___x86_64 = 1
const m___x86_64__ = 1
const m_alloca = "__builtin_alloca"
const m_linux = 1
const m_regcomp = "pcre2_regcomp"
const m_regerror = "pcre2_regerror"
const m_regexec = "pcre2_regexec"
const m_regfree = "pcre2_regfree"
const m_unix = 1

type t__builtin_va_list = uintptr

type t__predefined_size_t = uint64

type t__predefined_wchar_t = int32

type t__predefined_ptrdiff_t = int64

type Tlocale_t = uintptr

type Twchar_t = int32

type Tmax_align_t = struct {
	F__ll int64
	F__ld float64
}

type Tsize_t = uint64

type Tptrdiff_t = int64

type Tssize_t = int64

type Toff_t = int64

type Tva_list = uintptr

type t__isoc_va_list = uintptr

type Tfpos_t = struct {
	F__lldata [0]int64
	F__align  [0]float64
	F__opaque [16]int8
}

type T_G_fpos64_t = Tfpos_t

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tuintptr_t = uint64

type Tintptr_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tintmax_t = int64

type Tuint8_t = uint8

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Tuintmax_t = uint64

type Tint_fast8_t = int8

type Tint_fast64_t = int64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast64_t = uint64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast16_t = int32

type Tint_fast32_t = int32

type Tuint_fast16_t = uint32

type Tuint_fast32_t = uint32

type Timaxdiv_t = struct {
	Fquot Tintmax_t
	Frem  Tintmax_t
}

type TPCRE2_UCHAR8 = uint8

type TPCRE2_UCHAR16 = uint16

type TPCRE2_UCHAR32 = uint32

type TPCRE2_SPTR8 = uintptr

type TPCRE2_SPTR16 = uintptr

type TPCRE2_SPTR32 = uintptr

type Tpcre2_jit_callback_8 = uintptr

type Tpcre2_callout_block_8 = struct {
	Fversion               Tuint32_t
	Fcallout_number        Tuint32_t
	Fcapture_top           Tuint32_t
	Fcapture_last          Tuint32_t
	Foffset_vector         uintptr
	Fmark                  TPCRE2_SPTR8
	Fsubject               TPCRE2_SPTR8
	Fsubject_length        Tsize_t
	Fstart_match           Tsize_t
	Fcurrent_position      Tsize_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR8
	Fcallout_flags         Tuint32_t
}

type Tpcre2_callout_enumerate_block_8 = struct {
	Fversion               Tuint32_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_number        Tuint32_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR8
}

type Tpcre2_substitute_callout_block_8 = struct {
	Fversion        Tuint32_t
	Finput          TPCRE2_SPTR8
	Foutput         TPCRE2_SPTR8
	Foutput_offsets [2]Tsize_t
	Fovector        uintptr
	Foveccount      Tuint32_t
	Fsubscount      Tuint32_t
}

type Tpcre2_jit_callback_16 = uintptr

type Tpcre2_callout_block_16 = struct {
	Fversion               Tuint32_t
	Fcallout_number        Tuint32_t
	Fcapture_top           Tuint32_t
	Fcapture_last          Tuint32_t
	Foffset_vector         uintptr
	Fmark                  TPCRE2_SPTR16
	Fsubject               TPCRE2_SPTR16
	Fsubject_length        Tsize_t
	Fstart_match           Tsize_t
	Fcurrent_position      Tsize_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR16
	Fcallout_flags         Tuint32_t
}

type Tpcre2_callout_enumerate_block_16 = struct {
	Fversion               Tuint32_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_number        Tuint32_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR16
}

type Tpcre2_substitute_callout_block_16 = struct {
	Fversion        Tuint32_t
	Finput          TPCRE2_SPTR16
	Foutput         TPCRE2_SPTR16
	Foutput_offsets [2]Tsize_t
	Fovector        uintptr
	Foveccount      Tuint32_t
	Fsubscount      Tuint32_t
}

type Tpcre2_jit_callback_32 = uintptr

type Tpcre2_callout_block_32 = struct {
	Fversion               Tuint32_t
	Fcallout_number        Tuint32_t
	Fcapture_top           Tuint32_t
	Fcapture_last          Tuint32_t
	Foffset_vector         uintptr
	Fmark                  TPCRE2_SPTR32
	Fsubject               TPCRE2_SPTR32
	Fsubject_length        Tsize_t
	Fstart_match           Tsize_t
	Fcurrent_position      Tsize_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR32
	Fcallout_flags         Tuint32_t
}

type Tpcre2_callout_enumerate_block_32 = struct {
	Fversion               Tuint32_t
	Fpattern_position      Tsize_t
	Fnext_item_length      Tsize_t
	Fcallout_number        Tuint32_t
	Fcallout_string_offset Tsize_t
	Fcallout_string_length Tsize_t
	Fcallout_string        TPCRE2_SPTR32
}

type Tpcre2_substitute_callout_block_32 = struct {
	Fversion        Tuint32_t
	Finput          TPCRE2_SPTR32
	Foutput         TPCRE2_SPTR32
	Foutput_offsets [2]Tsize_t
	Fovector        uintptr
	Foveccount      Tuint32_t
	Fsubscount      Tuint32_t
}

const _REG_ASSERT = 1
const _REG_BADBR = 2
const _REG_BADPAT = 3
const _REG_BADRPT = 4
const _REG_EBRACE = 5
const _REG_EBRACK = 6
const _REG_ECOLLATE = 7
const _REG_ECTYPE = 8
const _REG_EESCAPE = 9
const _REG_EMPTY = 10
const _REG_EPAREN = 11
const _REG_ERANGE = 12
const _REG_ESIZE = 13
const _REG_ESPACE = 14
const _REG_ESUBREG = 15
const _REG_INVARG = 16
const _REG_NOMATCH = 17

type Tregex_t = struct {
	Fre_pcre2_code uintptr
	Fre_match_data uintptr
	Fre_endp       uintptr
	Fre_nsub       Tsize_t
	Fre_erroffset  Tsize_t
	Fre_cflags     int32
}

type Tregoff_t = int32

type Tregmatch_t = struct {
	Frm_so Tregoff_t
	Frm_eo Tregoff_t
}

/* Debian had a patch that used different names. These are now here to save
them having to maintain their own patch, but are not documented by PCRE2. */

/* End of pcre2posix.h */

/* Table to translate PCRE2 compile time error codes into POSIX error codes.
Only a few PCRE2 errors with a value greater than 23 turn into special POSIX
codes: most go to REG_BADPAT. The second table lists, in pairs, those that
don't. */

var _eint1 = [24]int32{
	1:  int32(_REG_EESCAPE),
	2:  int32(_REG_EESCAPE),
	3:  int32(_REG_EESCAPE),
	4:  int32(_REG_BADBR),
	5:  int32(_REG_BADBR),
	6:  int32(_REG_EBRACK),
	7:  int32(_REG_ECTYPE),
	8:  int32(_REG_ERANGE),
	9:  int32(_REG_BADRPT),
	10: int32(_REG_ASSERT),
	11: int32(_REG_BADPAT),
	12: int32(_REG_BADPAT),
	13: int32(_REG_BADPAT),
	14: int32(_REG_EPAREN),
	15: int32(_REG_ESUBREG),
	16: int32(_REG_INVARG),
	17: int32(_REG_INVARG),
	18: int32(_REG_EPAREN),
	19: int32(_REG_ESIZE),
	20: int32(_REG_ESIZE),
	21: int32(_REG_ESPACE),
	22: int32(_REG_EPAREN),
	23: int32(_REG_ASSERT),
}

var _eint2 = [12]int32{
	0:  int32(30),
	1:  int32(_REG_ECTYPE),
	2:  int32(32),
	3:  int32(_REG_INVARG),
	4:  int32(37),
	5:  int32(_REG_EESCAPE),
	6:  int32(56),
	7:  int32(_REG_INVARG),
	8:  int32(92),
	9:  int32(_REG_INVARG),
	10: int32(99),
	11: int32(_REG_EESCAPE),
}

/* Table of texts corresponding to POSIX error codes */

var _pstring = [18]uintptr{
	0:  __ccgo_ts,
	1:  __ccgo_ts + 1,
	2:  __ccgo_ts + 16,
	3:  __ccgo_ts + 44,
	4:  __ccgo_ts + 58,
	5:  __ccgo_ts + 72,
	6:  __ccgo_ts + 86,
	7:  __ccgo_ts + 100,
	8:  __ccgo_ts + 131,
	9:  __ccgo_ts + 141,
	10: __ccgo_ts + 161,
	11: __ccgo_ts + 178,
	12: __ccgo_ts + 192,
	13: __ccgo_ts + 212,
	14: __ccgo_ts + 231,
	15: __ccgo_ts + 252,
	16: __ccgo_ts + 271,
	17: __ccgo_ts + 284,
}

/*************************************************
*          Translate error code to string        *
*************************************************/
func Xpcre2_regerror(tls *libc.TLS, errcode int32, preg uintptr, errbuf uintptr, errbuf_size Tsize_t) (r Tsize_t) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var message, v1 uintptr
	var used int32
	_, _, _ = message, used, v1
	if errcode <= 0 || errcode >= libc.Int32FromUint64(libc.Uint64FromInt64(144)/libc.Uint64FromInt64(8)) {
		v1 = __ccgo_ts + 297
	} else {
		v1 = _pstring[errcode]
	}
	message = v1
	if preg != libc.UintptrFromInt32(0) && libc.Int32FromUint64((*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset) != -int32(1) {
		used = libc.X__builtin_snprintf(tls, errbuf, errbuf_size, __ccgo_ts+316, libc.VaList(bp+8, message, libc.Int32FromUint64((*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset)))
	} else {
		used = libc.X__builtin_snprintf(tls, errbuf, errbuf_size, __ccgo_ts+334, libc.VaList(bp+8, message))
	}
	return libc.Uint64FromInt32(used + int32(1))
}

/*************************************************
*           Free store held by a regex           *
*************************************************/
func Xpcre2_regfree(tls *libc.TLS, preg uintptr) {
	libpcre2_8.Xpcre2_match_data_free_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data)
	libpcre2_8.Xpcre2_code_free_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code)
}

/*************************************************
*            Compile a regular expression        *
*************************************************/

/*
Arguments:

	preg        points to a structure for recording the compiled expression
	pattern     the pattern to compile
	cflags      compilation flags

Returns:      0 on success

	various non-zero codes on failure
*/
func Xpcre2_regcomp(tls *libc.TLS, preg uintptr, pattern uintptr, cflags int32) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i uint32
	var options int32
	var patlen Tsize_t
	var v1 uint64
	var _ /* erroffset at bp+0 */ Tsize_t
	var _ /* errorcode at bp+8 */ int32
	var _ /* re_nsub at bp+12 */ int32
	_, _, _, _ = i, options, patlen, v1
	options = 0
	*(*int32)(unsafe.Pointer(bp + 12)) = 0
	if cflags&int32(m_REG_PEND) != 0 {
		v1 = libc.Uint64FromInt64(int64((*Tregex_t)(unsafe.Pointer(preg)).Fre_endp) - int64(pattern))
	} else {
		v1 = ^libc.Uint64FromInt32(0)
	}
	patlen = v1
	if cflags&int32(m_REG_ICASE) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000008))
	}
	if cflags&int32(m_REG_NEWLINE) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000400))
	}
	if cflags&int32(m_REG_DOTALL) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000020))
	}
	if cflags&int32(m_REG_NOSPEC) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x02000000))
	}
	if cflags&int32(m_REG_UTF) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00080000))
	}
	if cflags&int32(m_REG_UCP) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00020000))
	}
	if cflags&int32(m_REG_UNGREEDY) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00040000))
	}
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_cflags = cflags
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code = libpcre2_8.Xpcre2_compile_8(tls, pattern, patlen, libc.Uint32FromInt32(options), bp+8, bp, libc.UintptrFromInt32(0))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset = *(*Tsize_t)(unsafe.Pointer(bp))
	if (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code == libc.UintptrFromInt32(0) {
		/* A negative value is a UTF error; otherwise all error codes are greater
		than COMPILE_ERROR_BASE, but check, just in case. */
		if *(*int32)(unsafe.Pointer(bp + 8)) < int32(m_COMPILE_ERROR_BASE) {
			return int32(_REG_BADPAT)
		}
		*(*int32)(unsafe.Pointer(bp + 8)) -= int32(m_COMPILE_ERROR_BASE)
		if *(*int32)(unsafe.Pointer(bp + 8)) < libc.Int32FromUint64(libc.Uint64FromInt64(96)/libc.Uint64FromInt64(4)) {
			return _eint1[*(*int32)(unsafe.Pointer(bp + 8))]
		}
		i = uint32(0)
		for {
			if !(uint64(i) < libc.Uint64FromInt64(48)/libc.Uint64FromInt64(4)) {
				break
			}
			if *(*int32)(unsafe.Pointer(bp + 8)) == _eint2[i] {
				return _eint2[i+uint32(1)]
			}
			goto _2
		_2:
			;
			i += uint32(2)
		}
		return int32(_REG_BADPAT)
	}
	libpcre2_8.Xpcre2_pattern_info_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code, uint32(m_PCRE2_INFO_CAPTURECOUNT), bp+12)
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_nsub = libc.Uint64FromInt32(*(*int32)(unsafe.Pointer(bp + 12)))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data = libpcre2_8.Xpcre2_match_data_create_8(tls, libc.Uint32FromInt32(*(*int32)(unsafe.Pointer(bp + 12))+int32(1)), libc.UintptrFromInt32(0))
	(*Tregex_t)(unsafe.Pointer(preg)).Fre_erroffset = libc.Uint64FromInt32(-libc.Int32FromInt32(1)) /* No meaning after successful compile */
	if (*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data == libc.UintptrFromInt32(0) {
		/* LCOV_EXCL_START */
		libpcre2_8.Xpcre2_code_free_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code)
		return int32(_REG_ESPACE)
		/* LCOV_EXCL_STOP */
	}
	return 0
}

/*************************************************
*              Match a regular expression        *
*************************************************/

/*
	A suitable match_data block, large enough to hold all possible captures, was

obtained when the pattern was compiled, to save having to allocate and free it
for each match. If REG_NOSUB was specified at compile time, the nmatch and
pmatch arguments are ignored, and the only result is yes/no/error.
*/
func Xpcre2_regexec(tls *libc.TLS, preg uintptr, string1 uintptr, nmatch Tsize_t, pmatch uintptr, eflags int32) (r int32) {
	var eo, options, rc, so, v2, v3 int32
	var i Tsize_t
	var md, ovector uintptr
	var v5 Tregoff_t
	_, _, _, _, _, _, _, _, _, _ = eo, i, md, options, ovector, rc, so, v2, v3, v5
	options = 0
	md = (*Tregex_t)(unsafe.Pointer(preg)).Fre_match_data
	if string1 == libc.UintptrFromInt32(0) {
		return int32(_REG_INVARG)
	}
	if eflags&int32(m_REG_NOTBOL) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000001))
	}
	if eflags&int32(m_REG_NOTEOL) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000002))
	}
	if eflags&int32(m_REG_NOTEMPTY) != 0 {
		options = int32(uint32(options) | libc.Uint32FromUint32(0x00000004))
	}
	/* When REG_NOSUB was specified, or if no vector has been passed in which to
	   put captured strings, ensure that nmatch is zero. This will stop any attempt to
	   write to pmatch. */
	if (*Tregex_t)(unsafe.Pointer(preg)).Fre_cflags&int32(m_REG_NOSUB) != 0 || pmatch == libc.UintptrFromInt32(0) {
		nmatch = uint64(0)
	}
	/* REG_STARTEND is a BSD extension, to allow for non-NUL-terminated strings.
	   The man page from OS X says "REG_STARTEND affects only the location of the
	   string, not how it is matched". That is why the "so" value is used to bump the
	   start location rather than being passed as a PCRE2 "starting offset". */
	if eflags&int32(m_REG_STARTEND) != 0 {
		if pmatch == libc.UintptrFromInt32(0) {
			return int32(_REG_INVARG)
		}
		so = (*(*Tregmatch_t)(unsafe.Pointer(pmatch))).Frm_so
		eo = (*(*Tregmatch_t)(unsafe.Pointer(pmatch))).Frm_eo
	} else {
		so = 0
		eo = libc.Int32FromUint64(libc.Xstrlen(tls, string1))
	}
	rc = libpcre2_8.Xpcre2_match_8(tls, (*Tregex_t)(unsafe.Pointer(preg)).Fre_pcre2_code, string1+uintptr(so), libc.Uint64FromInt32(eo-so), uint64(0), libc.Uint32FromInt32(options), md, libc.UintptrFromInt32(0))
	/* Successful match */
	if rc >= 0 {
		ovector = libpcre2_8.Xpcre2_get_ovector_pointer_8(tls, md)
		if libc.Uint64FromInt32(rc) > nmatch {
			rc = libc.Int32FromUint64(nmatch)
		}
		i = uint64(0)
		for {
			if !(i < libc.Uint64FromInt32(rc)) {
				break
			}
			if *(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2))*8)) == ^libc.Uint64FromInt32(0) {
				v2 = -int32(1)
			} else {
				v2 = libc.Int32FromUint64(*(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2))*8)) + libc.Uint64FromInt32(so))
			}
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_so = v2
			if *(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2)+uint64(1))*8)) == ^libc.Uint64FromInt32(0) {
				v3 = -int32(1)
			} else {
				v3 = libc.Int32FromUint64(*(*Tsize_t)(unsafe.Pointer(ovector + uintptr(i*uint64(2)+uint64(1))*8)) + libc.Uint64FromInt32(so))
			}
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_eo = v3
			goto _1
		_1:
			;
			i++
		}
		for {
			if !(i < nmatch) {
				break
			}
			v5 = -libc.Int32FromInt32(1)
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_eo = v5
			(*(*Tregmatch_t)(unsafe.Pointer(pmatch + uintptr(i)*8))).Frm_so = v5
			goto _4
		_4:
			;
			i++
		}
		return 0
	}
	/* Unsuccessful match */
	if rc <= -int32(3) && rc >= -int32(23) {
		return int32(_REG_INVARG)
	}
	/* Most of these are events that won't occur during testing, so exclude them
	   from coverage. */
	switch rc {
	case -int32(63):
		return int32(_REG_ESPACE)
	case -int32(1):
		return int32(_REG_NOMATCH)
		/* LCOV_EXCL_START */
		fallthrough
	case -int32(32):
		return int32(_REG_INVARG)
	case -int32(31):
		return int32(_REG_INVARG)
	case -int32(34):
		return int32(_REG_INVARG)
	case -int32(36):
		return int32(_REG_INVARG)
	case -int32(47):
		return int32(_REG_ESPACE)
	case -int32(48):
		return int32(_REG_ESPACE)
	case -int32(51):
		return int32(_REG_INVARG)
	default:
		return int32(_REG_ASSERT)
		/* LCOV_EXCL_STOP */
	}
	return r
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\x00internal error\x00invalid repeat counts in {}\x00pattern error\x00? * + invalid\x00unbalanced {}\x00unbalanced []\x00collation error - not relevant\x00bad class\x00bad escape sequence\x00empty expression\x00unbalanced ()\x00bad range inside []\x00expression too big\x00failed to get memory\x00bad back reference\x00bad argument\x00match failed\x00unknown error code\x00%s at offset %-6d\x00%s\x00"
